// JavaScript Document
const storeList = [
  {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: [-85.62449174465479,42.9751332263572],
    },
    properties: {
      name: "Electrode Dressers, Inc.",
      address: "686 Plymouth Ave NE, Grand Rapids, MI 49505",
      phone: "616-538-6224",
      website: "https://electrodedressers.com",
    },
  },
  {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: [-85.20050462183251,35.026590364624155],
    },
    properties: {
      name: "T. J. Snow",
      address: "120 Nowlin Lane, Chattanooga, TN 37421",
      phone: "800-669-7669",
      website: "https://spotweldingsupplies.com",
    },
  },
  {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: [-82.23982412563787,34.8620791963465],
    },
    properties: {
      name: "CMW Inc. (Tuffaloy Products, Inc.)",
      address:
        "1400 Batesville Road, Greer, SC 29650",
      phone: "800-521-3722",
      website: "https://cmwinc.com",
    },
  },
  {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: [-84.12061317529981,39.7412997422191],
    },
    properties: {
      name: "Production Engineering",
      address: "1344 Woodman Drive, Dayton, OH 45432",
      phone: "937-253-3300",
      website: "https://resistanceweldsupplies.com",
    },
  },
  {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: [-81.53230124536276,41.42550028133251],
    },
    properties: {
      name: "Weld Systems Integrators",
      address: "4943 Driscoll Road, Warrensville Heights, OH 44146",
      phone: "844-974-9353",
      website: "https://wsiweld.com",
    },
  },
  {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: [-83.0578573296584,42.37465478281438],
    },
    properties: {
      name: "Tipaloy, Inc.",
      address:
        "1435 E Milwaukee Ave., Detroit, MI 48211",
      phone: "313-875-5145",
      website: "https://tipaloy.com",
    },
  },
  {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: [-96.56172648382622,32.90653118897042],
    },
    properties: {
      name: "Spotwelding Consultants Inc.",
      address:
        "4209 Industrial St., Rowlett, Texas 75088",
      phone: "888-255-6780",
      website: "https://spotweldingconsultants.com",
    },
  }, 
];
