OpenSreetMaps / Leaflet web page. Modified slightly to provide a dealer locator page with additional popup details for linked websites, linked 'tel:' phone links, and added geolocation zoom-to functions from the left-edge nav column.

### Original code:
Forked from: https://github.com/dev-manindepth/store-locator-leafletMaps

Original demo: https://dev-manindepth.github.io/store-locator-leafletMaps/

### To-Do:

Rework the CSS to gracefully handle a slide-out nav column, and be more mobile friendly.