// JavaScript Document
//const myHomeCord = [42.97513853746586, -85.6245004889963];
const myHomeCord = [38.24971170337246, -85.76382907355536];
const map = L.map("map").setView(myHomeCord, 6);

// Base layer
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 19,
  minZoom: 3,
  noWrap: true,
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> | <a href="https://www.openstreetmap.org/fixthemap">Report or FixTheMap</a>',
}).addTo(map);
// const marker = L.marker([42.97513853746586, -85.6245004889963]).addTo(map);

// const popup = L.popup({
//     offset: L.point(0, 0)
//   })
//   .setLatLng([42.97513853746586, -85.6245004889963])
//   .setContent("<p>Electrode Dressers, Inc.</p>")
//   .openOn(map);

function generateList() {
  const ul = document.querySelector(".list");
  storeList.forEach((store) => {
    const li = document.createElement("li");
    const div = document.createElement("div");
    const a = document.createElement("a");
    const p = document.createElement("p");

    div.classList.add("shopItem");
    a.innerText = store.properties.name;
    a.href = "#";
    a.addEventListener("click", () => {
      flyToStore(store);
    });
    p.innerHTML = store.properties.address;

    div.appendChild(a);
    div.appendChild(p);
    li.appendChild(div);
    ul.appendChild(li);
  });
}

generateList();

console.log();

function makePopupContent(shop) {

  console.log(shop.properties);
  return `
        <div class="locationDiv">
            <h4>${shop.properties.name}</h4>
            <p>${shop.properties.address}</p>

            <div class="phoneNumber">
                Phone: <a href="tel:${shop.properties.phone.replace(/-/gi,"")}">${shop.properties.phone}</a>
            </div>
            <div class="siteLink">
                Site: <a target="_blank" href="${shop.properties.website}">${shop.properties.website}</a>
            </div>            
        </div>
    `;
}

function onEachFeature(feature, layer) {
  layer.bindPopup(makePopupContent(feature), {
    closeButton: false,
    offset: L.point(0, -8),
  });
}
const myIcon = L.icon({
  iconUrl: "./marker.png",
  iconSize: [25, 50],
  iconAnchor: [12.5, 50],
  popupAnchor: [0, -25],
});

const shopLayer = L.geoJSON(storeList, {
  onEachFeature: onEachFeature,
  pointToLayer: function(feature, latlng) {
    return L.marker(latlng, {
      icon: myIcon
    });
  },
});

shopLayer.addTo(map);

function flyToStore(store) {
  const cord = [store.geometry.coordinates[1], store.geometry.coordinates[0]];
  map.flyTo([...cord], 11, {
    duration: 3,
  });

  setTimeout(() => {
    L.popup({
        closeButton: false,
        offset: L.point(0, -35)
      })
      .setLatLng([...cord])
      .setContent(makePopupContent(store))
      .openOn(map);
  }, 3000);
}